# Jeu du Loto

Ce projet impl�mente un jeu de loto associatif, appel� aussi Quine ou Rifle. Il permet 3 types de jeu diff�rents : la ligne gagnante, le carton plein et le carton vide. Il s'agit d'une application simplifi� utilisable via la console.

## R�gles du loto 


### Le tirage

Chaque joueur est muni d�un carton ou plusieurs cartons de loto et de pions. Parmi 90 boules num�rot�es de 1 � 90, un num�ro est tir� au sort et le joueur place son pion sur le num�ro sorti s�il est mentionn� sur son carton.

### Les diff�rents modes de jeu

La partie est gagn�e � l'issue d'une combinaison de num�ro sur la carton du joueur. Cette combinaison est d�termin�e par le mode de jeu choisi.

* **ligne gagnante** : la partie est remport�e par le premier joueur ayant marqu� une ligne compl�te.
* **carton plein** : la partie est remport�e par le premier joueur ayant marqu� tous les num�ros de son carton.
* **carton vide** : la partie est remport�e par le dernier joueur sans aucun num�ro marqu� sur son carton.

### Les cartons

Les cartons sont compos�s 15 num�ros agenc�s dans un tableau de 3 lignes sur 9 colonnes.

Quelques r�gles dans la composition d'un carton :

* Chaque colonne repr�sente une dizaine.
* Chaque ligne poss�de 5 num�ros sans exception. Les 4 autres cases sont laiss�es vierges.
* Chaque colonne � au moins 1 num�ro. 

Exemple d'un carton : [https://www.lepalaisduloto.fr/609/1000-cartons-de-loto-200-gr.jpg](https://www.lepalaisduloto.fr/609/1000-cartons-de-loto-200-gr.jpg)


## Installation

T�l�charger le d�p�t Git sur votre ordinateur.

```bash
git clone git@gitlab.com:bastien-angles/loto.git
```

Lancer le projet dans votre IDE Java et ex�cuter la fonction `main` de la classe `Main.java`.

Un retour console vous permettra d�ex�cuter le jeu.

## Utilisation

Le jeu poss�de diverses fonctionnalit�s qui peuvent �tre �dit�es depuis la classe `Main.java`.

* Ajouter des joueurs avec un ou plusieurs cartons.
* Modifier le type de jeu : ligne gagnante, carton plein, carton vide.
* Tirer un num�ro manuellement.
* Tirer un num�ro au hasard.
* Annuler un num�ro tir� pr�c�demment.

## Licence
[MIT](https://choosealicense.com/licenses/mit/)