package com.main.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Draw {

	
	private List<Integer> drawList;
	private List<Player> players;
	
	/*
	 * For game attribute :
	 * 1 - Win with line full marked
	 * 2 - Win with cardboard full marked
	 * 3 - Win with full empty cardboard
	 */
	private int game;

	public Draw(List<Player> players, int game) {
		this.drawList = new ArrayList<Integer>();
		this.players = players;
		this.game = game;
		
		// Mark off all players cards
		this.markOff();
	}

	/*
	 * Draw a new random number
	 */
	public Player drawRandom() {
		Player winner = null;
		int lower = 1; 
		int higher = 90;
		int random = new Random().nextInt((higher-lower) + 1) + lower;
		if(!this.drawList.contains(random)) {
			this.drawList.add(random);
			this.markNumber(random);
			winner = this.checkWinner();
		}else{
			return drawRandom();
		}
		return winner;
	}

	/*
	 * Draw a new number
	 */
	public Player drawNumber(int number) {
		Player winner = null;
		if(number > 0 && number <= 90) {
			if(!this.drawList.contains(number)) {
				this.drawList.add(number);
				this.markNumber(number);
				winner = this.checkWinner();
			}else{
				System.out.println("Le num�ro a d�j��t� tir�.");
			}
		} else {
			System.out.println("Le num�ro tir� doit �tre compris entre 1 et 90.");
		}
		return winner;
	}
	
	/*
	 * Check winners
	 */
	public Player checkWinner() {
		switch(this.game) {
			case 1:
				Player winnerLine = this.winningLine();
				if(winnerLine != null) {
					System.out.println(("Gagnant ligne "+winnerLine.getFirstName()).toUpperCase());
					System.out.println("");
					return winnerLine;
				}
				break;
			case 2:
				Player winnerFull = this.winningFull();
				if(winnerFull != null) {
					System.out.println(("Gagnant carton plein "+winnerFull.getFirstName()).toUpperCase());
					System.out.println("");
					return winnerFull;
				}
				break;
			case 3:
				Player winnerEmpty = this.winningEmpty();
				if(winnerEmpty != null) {
					System.out.println(("Gagnant carton vide "+winnerEmpty.getFirstName()).toUpperCase());
					System.out.println("");
					return winnerEmpty;
				}
				break;
		}
		return null;
	}

	/*
	 * Mark number on each player boards
	 */
	public void markNumber(int number) {
		for(int i = 0; i < this.players.size(); i++) {
			this.players.get(i).markNumber(number);
		}
	}
	
	/*
	 * Mark off number on each player boards
	 */
	public void markOff() {
		for(int i = 0; i < this.players.size(); i++) {
			this.players.get(i).markOff();
			this.players.get(i).setIgnored(false);
		}
	}
	
	/*
	 * Remove number on each player boards
	 */
	public void removeNumber(int number) {
		if (this.getNumbers().contains(number)) {
			for(int i = 0; i < this.players.size(); i++) {
				this.players.get(i).pastCardBoards(number, this.getNumbers());
			}
			this.drawList.remove(new Integer(number));
		} else {
			System.out.println("Le num�ro "+number+" n'a jamais �t� tir�.");
		}
	}

	/*
	 * Check if players have a winning line
	 */
	public Player winningLine() {
		List<Player> winners = new ArrayList<Player>();
		for(int i = 0; i < this.players.size(); i++) {
			if(!this.players.get(i).isIgnored() && this.players.get(i).checkWinningLine()) {
				winners.add(this.players.get(i));
			}
		}
		if(winners.size() > 0) {
			int index = new Random().nextInt(winners.size());
			return winners.get(index);
		}
		return null;
	}

	/*
	 * Check if players have a full board
	 */
	public Player winningFull() {
		List<Player> winners = new ArrayList<Player>();
		for(int i = 0; i < this.players.size(); i++) {
			if(!this.players.get(i).isIgnored() && this.players.get(i).checkWinningFull()) {
				winners.add(this.players.get(i));
			}
		}
		if(winners.size() > 0) {
			int index = new Random().nextInt(winners.size());
			return winners.get(index);
		}
		return null;
	}

	/*
	 * Check if there is only one player left with an empty card
	 */
	public Player winningEmpty() {
		Player winner = null;
		for(int i = 0; i < this.players.size(); i++) {
			if(!this.players.get(i).isIgnored() && this.players.get(i).checkFullEmpty()) {
				if(winner == null) {
					winner = this.players.get(i);
				}else {
					return null;
				}
			}
		}
		/*
		 * In case no player has empty cards, the players with the last number 
		 * on the card are selected and one of these players is drawn at random.
		 */
		if(winner == null) {
			List<Player> possibleWinners = new ArrayList<Player>();
			for(int i = 0; i < this.players.size(); i++) {
				List<CardBoard> pastCards = this.players.get(i).pastCardBoards(this.drawList.get(this.drawList.size()-1), this.drawList);
				for(int j = 0; j < pastCards.size(); j++) {
					CardBoard card = pastCards.get(j);
					if(card.checkFullEmpty()) {
						possibleWinners.add(this.players.get(i));
					}
				}
			}
			int random = new Random().nextInt(possibleWinners.size());
			winner = possibleWinners.get(random);
		}
		return winner;
	}

	/*
	 * Getters and setters
	 */
	public List<Integer> getNumbers() {
		return drawList;
	}

	public void setNumbers(List<Integer> numbers) {
		this.drawList = numbers;
	}
	
	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}


	/*
	 * Display functions
	 */
	public void drawList() {
		Collections.sort(this.drawList);
		System.out.println(this.drawList);
	}

}
