package com.main.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Line {
	
	private List<Integer> line;

	public Line(List<Integer> numbers) {
		this.line = new ArrayList<Integer>(9);
		numbers.forEach(i -> this.line.add(i));
	}
	
	/*
	 * Check if line contain specific number
	 */
	public boolean contain(int number) {
		if(this.line.contains(number)) {
			return true;
		}
		return false;
	}
	
	/*
	 * Counts the occurrences of a specific number on a line
	 */
	public int countOccurs(int number) {
		int size = 0;
		for(int i = 0; i < this.line.size(); i++) {
			if(this.line.get(i) == number) {
				size = size+1;
			}
		}
		return size;
	}
	
	/*
	 * Return the position of a specific number
	 */
	public int indexOf(int number) {
		return this.line.indexOf(number);
	}
	
	/*
	 * Returns the number of full cells on a line
	 */
	public int size() {
		return this.line.size() - Collections.frequency(this.line, 0);
	}
	
	/*
	 * Retrieves a random number from a line
	 */
	public int extractOverflowNumber() {
		int index = new Random().nextInt(10);
		while(this.line.get(index) == 0) {
			index = new Random().nextInt(10);
		}
		int number = this.line.get(index);
		this.line.set(index, 0);
		return number;
	}

	/*
	 * Init a line of the checker board
	 * - 0 : the cell contain a number
	 * - 1 : the cell contain a white space
	 */
	public Line initChecker() {
		List<Integer> checkerLine = new ArrayList<Integer>(9);
		for(int i = 0; i < this.line.size(); i++) {
			if(this.line.get(i) == 0) {
				checkerLine.add(1);
			}else {
				checkerLine.add(0);
			}
		}
		return new Line(checkerLine);
	}
	
	/*
	 * Getters and setters
	 */
	public List<Integer> getLine() {
		return line;
	}

	public void setLine(List<Integer> line) {
		this.line = line;
	}
	
	public void setNumber(int position, int number) {
		this.line.set(position, number);
	}
	

}
