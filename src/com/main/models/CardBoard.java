package com.main.models;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CardBoard {
	
	private List<Line> card;
	private List<Line> checkerBoard;
	
	/*
	 * Constructor
	 */
	public CardBoard() {
		//Generate a list of 15 digits
		List<Integer> dataCard = new ArrayList<Integer>();
		for(int i = 0; i < 15; i++) {
			dataCard = newNumberCardBoard(dataCard);
		}
		// Divide numbers in lines
		this.card = arrangeToLines(dataCard, 1);
		// Init checkerBoard
		this.initCheckerBoard();
	}
	
	/*
	 * Init checker board
	 * 1 - number drawn | 0 - number not yet drawn
	 */
	private List<Line> initCheckerBoard() {
		this.checkerBoard = new ArrayList<Line>(3);
		for(int i = 0; i < this.card.size(); i++) {
			this.checkerBoard.add(this.card.get(i).initChecker());
		}
		return this.checkerBoard;
	}
	
	
	/*
	 * Arrange card in lines
	 */
	private List<Line> arrangeToLines(List<Integer> dataCard, int lvl) {
		// Retrieve columns
		List<List<Integer>> columns = new ArrayList<List<Integer>>(9);
		for(int i = 0; i < 9; i++) {
			List<Integer> column = new ArrayList<Integer>(3);
			// Get dozen numbers to column
			int dozen = i*10;
			column = getDozenNumbers(dataCard, dozen);
			// If column has less 3 numbers, add 0 to complete
			while(column.size() != 3) {
				column.add(0);
			}
			// Shuffle column
			Collections.shuffle(column);
			// Add this column to CardBoard columns
			columns.add(column);
		}
		// Retrieve lines
		List<Line> tempCard = this.makeLines(columns);
		if(tempCard.get(0).size() != 5 || tempCard.get(1).size() != 5 || tempCard.get(2).size() != 5) {
			tempCard = this.arrangeToLines(dataCard, (lvl+1));
		}
		return tempCard;
	}
	
	
	/*
	 * Arrange numbers on correct dozen
	 */
	private List<Integer> getDozenNumbers(List<Integer> dataCard, int dozen) {
		List<Integer> list = new ArrayList<Integer>(3);
		// Put number in correct dozen
		for(int i = 0; i < dataCard.size(); i++) {
			int number = dataCard.get(i);
			if((dozen <= number && number < dozen+10) || (dozen == 80 && number == 90)) {
				list.add(number);
			}
		}
		return list;
	}


	/*
	 * Add number on cardBoard
	 */
	private List<Integer> newNumberCardBoard(List<Integer> card) {
		int lower = 1; 
		int higher = 90;
		int random = new Random().nextInt((higher-lower) + 1) + lower;
		if(!card.contains(random) && getDozenLength(card, random) < 3) {
			card.add(random);
			return card;
		}else{
			return newNumberCardBoard(card);
		}
	}
	
	
	/*
	 * Get dozen number's in Collection of a focus number
	 */
	private int getDozenLength(List<Integer> card, int focus) {
		int length = 0;
		int quotient = focus/10;
		if(focus == 90) {
			quotient = 8;
		}
		for(int i = 0; i < card.size(); i++){
			if(card.get(i)/10 == quotient || (card.get(i) == 90 && quotient == 8)) {
				length++;
			}
		}
		return length;
	}
	
	
	/*
	 * Make lines with columns
	 */
	private List<Line> makeLines(List<List<Integer>> columns){
		List<Line> card = new ArrayList<Line>(3);
		// Insert columns numbers in lines
		for(int j = 0; j < 3; j++) {
			List<Integer> numbersLine = new ArrayList<Integer>(9);
			for(int i = 0; i < columns.size(); i++) {
				numbersLine.add(columns.get(i).get(j));
			}
			Line line = new Line(numbersLine);
			card.add(line);
		}
		return card;
	}
	
	
	/*
	 * Check if card board contain a specific number
	 */
	public boolean containCard(int number) {
		for(int i = 0; i < this.card.size(); i++) {
			if(this.card.get(i).contain(number)){
				return true;
			}
		}
		return false;
	}
	
	
	/*
	 * Check if checker board contain a specific number
	 */
	public boolean containChecker(int number) {
		for(int i = 0; i < this.checkerBoard.size(); i++) {
			if(this.checkerBoard.get(i).contain(number)){
				return true;
			}
		}
		return false;
	}
		
	
	/*
	 * Check if card board has a winning line
	 */
	public boolean checkWinningLine() {
		for(int i = 0; i < this.checkerBoard.size(); i++) {
			if(!this.checkerBoard.get(i).contain(0)){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Check if card board is full marked
	 */
	public boolean checkWinningFull() {
		for(int i = 0; i < this.checkerBoard.size(); i++) {
			if(this.checkerBoard.get(i).contain(0)){
				return false;
			}
		}
		return true;
	}
	
	/*
	 * Check if card board is full empty
	 */
	public boolean checkFullEmpty() {
		int unmarkedLength = 0;
		for(int i = 0; i < this.checkerBoard.size(); i++) {
			unmarkedLength = unmarkedLength + this.checkerBoard.get(i).countOccurs(0);
		}
		if(unmarkedLength == 15) {
			return true;
		}
		return false;
	}
	
	
	/*
	 * Mark number in checker board
	 */
	public void mark(int drawNumber) {
		for(int i = 0; i < this.card.size(); i++) {
			Line line = this.card.get(i);
			if(line.contain(drawNumber)){
				int index = line.indexOf(drawNumber);
				this.checkerBoard.get(i).setNumber(index, 1);
			}
		}
	}
	
	/*
	 * Mark off number in checker board
	 */
	public void markOff(int number) {
		for(int i = 0; i < this.card.size(); i++) {
			Line line = this.card.get(i);
			if(line.contain(number)){
				int index = line.indexOf(number);
				this.checkerBoard.get(i).setNumber(index, 0);
			}
		}
	}
	
	/*
	 * Mark off all numbers in checker board
	 */
	public void markOffAll() {
		this.initCheckerBoard();
	}
	
	/*
	 * Count empty cells
	 */
	public int countEmptyCells() {
		int empty = 0;
		for(int i = 0; i < this.checkerBoard.size(); i++) {
			empty += this.checkerBoard.get(i).countOccurs(0);
		}
		return empty;
	}

	/*
	 * Getters and Setters
	 */
	public List<Line> getCard() {
		return card;
	}


	public void setCard(List<Line> card) {
		this.card = card;
	}


	public List<Line> getCheckerBoard() {
		return checkerBoard;
	}


	public void setCheckerBoard(List<Line> checkerBoard) {
		this.checkerBoard = checkerBoard;
	}
	
	
	/*
	 * Display functions
	 */
	public void displayCard() {
		this.card.forEach(l -> System.out.println(l.getLine()));
	}
	
	public void displayCheckerBoard() {
		this.checkerBoard.forEach(l -> System.out.println(l.getLine()));
	}

}
