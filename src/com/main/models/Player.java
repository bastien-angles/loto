package com.main.models;

import java.util.ArrayList;
import java.util.List;

public class Player {
	
	private List<CardBoard> cards;
	private String firstName;
	private String lastName;
	private int nbCards;
	private int remainingNumbers;
	private boolean ignored;
	
    public Player() {
        this(0, null, null);
    }

	public Player(int nbCard, String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.nbCards = 0;
		this.remainingNumbers = 0;
		this.setIgnored(false);
		this.createCardBoard(nbCard);
	}
	
	private void addCard(int nbCard) {
		for(int i = 0; i < nbCard; i++) {
			this.cards.add(new CardBoard());
		}
		this.setNbCard();
		this.setRemainingNumbers();
	}
	
	/*
	 *	Create card boards
	 */
	public void createCardBoard(int nbCard) {
		this.cards = new ArrayList<CardBoard>();
		this.addCard(nbCard);
	}
	
	/*
	 * Mark drawn number if exist on player cards
	 */
	public void markNumber(int drawNumber) {
		for(int i = 0; i < this.cards.size(); i++) {
			CardBoard card = this.cards.get(i);
			if(card.containCard(drawNumber)) {
				card.mark(drawNumber);
			}
		}
		// Update remaining numbers
		this.setRemainingNumbers();
	}
	
	/*
	 * Mark off numbers on all cards
	 */
	public void markOff() {
		for(int i = 0; i < this.cards.size(); i++) {
			this.cards.get(i).markOffAll();
		}
		this.setRemainingNumbers();
	}
	
	/*
	 * Check if one the cardboard has a winning line
	 */
	public boolean checkWinningLine() {
		for(int i = 0; i < this.cards.size(); i++) {
			CardBoard card = this.cards.get(i);
			if(card.checkWinningLine()) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Check if one the cardboard is full marked
	 */
	public boolean checkWinningFull() {
		for(int i = 0; i < this.cards.size(); i++) {
			CardBoard card = this.cards.get(i);
			if(card.checkWinningFull()) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Check if one the cardboard is full empty
	 */
	public boolean checkFullEmpty() {
		for(int i = 0; i < this.cards.size(); i++) {
			CardBoard card = this.cards.get(i);
			if(card.checkFullEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	/* 
	 * Get cardboards before specific number drawn
	 * - removes all numbers that have been drawn after him 
	 */
	public List<CardBoard> pastCardBoards(int number, List<Integer> drawList) {
		List<CardBoard> pastCards = this.cards;
		for(int i = drawList.size() - 1 ; i >= drawList.indexOf(number); i--) {
			int numberDrawn = drawList.get(i); 
			for(int j = 0; j < pastCards.size(); j++) {
				CardBoard card = this.cards.get(j);
				if(card.containCard(numberDrawn)) {
					card.markOff(numberDrawn);
				}
			}
		}
		return pastCards;
	}
	
	/*
	 * Get player cards
	 */
	public List<CardBoard> getCards() {
		return cards;
	}

	public void setCards(List<CardBoard> cards) {
		this.cards = cards;
	}
	
	/*
	 * Calculate remaining numbers
	 */
	private int calcRemainingNumbers() {
		int remaining = 15;
		for(CardBoard card : this.getCards()) {
			if(card.countEmptyCells() < remaining) {
				remaining = card.countEmptyCells();
			}
		}
		return remaining;
	}

	/*
	 * Getters and setters
	 */
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public int getNbCard() {
		return this.nbCards;
	}
	
	public void setNbCard() {
		this.nbCards = this.cards.size();
	}	
	
	public int getRemainingNumbers() {
		return this.remainingNumbers;
	}
	
	public void setRemainingNumbers() {
		this.remainingNumbers = this.calcRemainingNumbers();
	}

	public boolean isIgnored() {
		return ignored;
	}

	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}

}
