
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.main.models.Draw;
import com.main.models.Player;

public class Main {
	
	public static void main(String[] args) {
		
		List<Player> players = new ArrayList<Player>();
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < 1; i++) {
			players.add(new Player(1, "Bastien", "Angles"));
			players.add(new Player(1, "Francis", "Aukey"));
		}
		
		System.out.println("------ CARTONS (0 = case vide) -------");
		
		System.out.println("Joueur 1 :");
		players.get(0).getCards().forEach(c -> c.displayCard());
		
		System.out.println("Joueur 2 :");
		players.get(1).getCards().forEach(c -> c.displayCard());
		
		// Modifier le type de jeu - second param�tre du constructeur
		// 1 - Gagnant par ligne
		// 2 - Gagnant carton plein
		// 3 - Gagnant carton vide
		Draw draw = new Draw(players, 1);
		
		int choice = 1;
		while(choice != 3) {
			choice = gameMenu(sc);
			switch(choice) {
				case 1:
					drawNumber(sc, draw);
					break;
				case 2:
					removeNumber(sc, draw);
					break;
				case 3:
					System.out.println("");
					System.out.println("Partie termin�e sans gagnant.");
					System.out.println("Num�ro tir�s:");
					draw.drawList();
					showCheckerBoards(draw);
					System.exit(0);
					break;
				default:
					System.out.println("Veuillez choisir un chiffre entre 1-3");
					break;
			}
			
		}
		
		sc.close();
	}
	
	public static int gameMenu(Scanner sc) {
		System.out.println("");
		System.out.println("---------------- MENU ----------------");
		System.out.println("1 - Tirer un nouveau num�ro");
		System.out.println("2 - Annuler un num�ro");
		System.out.println("3 - Arr�ter la partie");
		System.out.println("Que voulez-vous faire ?");
		return sc.nextInt();
	}
	
	public static void drawNumber(Scanner sc, Draw draw) {
		System.out.println("");
		System.out.println("Saisissez le num�ro � tirer:");
		int input = sc.nextInt();
		Player winner = draw.drawNumber(input);
		System.out.println("");
		System.out.println("Num�ro tir�s:");
		draw.drawList();
		
		showCheckerBoards(draw);
		
		if (winner != null) {
			System.exit(0);
		}
	}
	
	public static void removeNumber(Scanner sc, Draw draw) {
		System.out.println("");
		System.out.println("Saisissez le num�ro � retirer:");
		int input = sc.nextInt();
		draw.removeNumber(input);
		System.out.println("");
		System.out.println("Num�ro tir�s:");
		draw.drawList();
		
		showCheckerBoards(draw);
	}
	
	public static void showCheckerBoards(Draw draw) {
		System.out.println("");
		System.out.println("------ STATUT DES CARTONS ( 1 = case pleine / 0 = case vide) -------");
		System.out.println("Joueur 1 :");
		draw.getPlayers().get(0).getCards().forEach(c -> c.displayCheckerBoard());
		
		System.out.println("Joueur 2 :");
		draw.getPlayers().get(1).getCards().forEach(c -> c.displayCheckerBoard());
	}

}
